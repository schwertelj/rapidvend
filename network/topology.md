# Rapidvend Network Topology

## Background

The farm that rapidvend sits on does not have an internet line running to it, so remote connectivity and IoT capabilities to the vending machines was not trivial. As R&D and troubleshooting continued, it became increasingly clear how useful a stable internet connection to all vending machines would be.

## Getting Internet to the Farm
Rather than get an ISP to run a line to the farm, we went with a T-Mobile 5g Router (https://www.t-mobile.com/isp/gateway) which provided ample speed and bandwidth. It is, however, inteneded for home use and had a very small connection radius.

## Distributing Internet
The vending machines sit ~100m away from the main farm shop, across an open gravel lot. Line of sight to the main shop is good but does not scale well to more than one vending machine. Thus, a mesh network of TP-Link EAP-225s (https://www.amazon.com/gp/product/B07953S2FD/ref=ppx_yo_dt_b_asin_title_o08_s00?ie=UTF8&psc=1) were used to cover the entire area that vending machines spread across


![](rv_network.png)

## DHCP + EAP Management

Shockingly, the TMobile router was really not designed for supporting an external mesh network, so a Raspberry Pi 4 running TP-Link's controller software (https://www.tp-link.com/us/support/download/omada-software-controller/) was setup to manage the access points.

Because of this, the EAPs couldn't user the TMobile router's DNS abilities so the RPi4 acts as a DHCP server.

## Getting Internet to the Vending Machines

The Raspberry Pi's on-board wifi antenna is unsuprisingly weak and nearly incapable of going through the 1/4" steel case of the vending machine. Adding a USB wifi antenna (https://www.amazon.com/gp/product/B00H95C0A2/ref=ppx_yo_dt_b_search_asin_title?ie=UTF8&psc=1) improved signal strength significantly.

## Remote Access

Yet another shortcoming of the TMobile router is that it cannot port forward like a normal home router. Thus, accessing the vending machines was not as simple as a firect SSH connection.

Luckily, the RPi4 comes to the rescue again and acts as a reverse SSH proxy to the vending machines. The RPI4 reverse SSHs to the rapidvend cloud server, and because it is on the same network as the vending machines, it can ssh into them directly.
