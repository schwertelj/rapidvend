#include "wire.h"
#include "LiquidCrystal_I2C.h"
#include "augerSub.h"

void vend()
{
  
  if (toggleState != lastToggleState)
  {
    //last_dollar_time = millis();

         int tilt = digitalRead(tiltSwitch);
          if(tilt == 0);
            {
              lcd.clear();
              digitalWrite(augerEnable, LOW);
              remainAugerPulse=paidAugerPulse; //saves the remaining pulses that were paid for
              break;
            }
    if (paidAugerPulse > 0)
      {
      dollarCounter = 0;
      LiquidCrystal_I2C lcd(lcdAddr,lcdCols,lcdRows);
      lcd.init();
      while (paidAugerPulse > 0)
        {
        //LiquidCrystal_I2C lcd(lcdAddr,lcdCols,lcdRows);
        //lcd.init();
        dollarCounter = 0; //set dollar counter to 0
        digitalWrite(augerEnable, HIGH); //turn auger on
        lcd.backlight();
        lcd.setCursor(5,0);
        lcd.print(F("Vending..."));
        lcd.setCursor(1,1);
        lcd.print(F("Pounds left: "));
        lcd.print(paidAugerPulse*pulseAdj);
        remainAugerPulse = paidAugerPulse;
        
          if (paidAugerPulse*pulseAdj < 100)
            {
              lcd.setCursor(19,1);
              lcd.print(F(" "));
            }
          if (paidAugerPulse*pulseAdj < 10)
            {
              lcd.setCursor(18,1);
              lcd.print(F(" "));
            }

        toggleState = digitalRead(handToggleSwitch); //read toggle state
          if (toggleState == lastToggleState) //if user shuts off the corn flow to switch bags
            {
              lcd.clear();
              digitalWrite(augerEnable, LOW);
              remainAugerPulse=paidAugerPulse; //saves the remaining pulses that were paid for
              break;
            }
          delay(0);
          }

      lastToggleState = toggleState; //sets current state as the last state, don't need to switch on or off
      digitalWrite(augerEnable, LOW); //turn off auger
      }

    else if (paidAugerPulse == 0) //if the paid auger pulses is 0, print no credits line
      {
        lastToggleState = toggleState;
        remainAugerPulse = 0;
      }

    digitalWrite(augerEnable, LOW);
  }
}
