n//Defined Pins

#define billValidator 2
#define handToggleSwitch 5
#define augerPulse 3
#define augerEnable 8
#define Output_PIN 4
#define tiltSwitch 7

//Defined Variables

int dollarCounter = 0;
int dollarIn;
int toggleState = 0;
int lastToggleState = 0;
int augerPulseCounter = 0;
float paidAugerPulse = 0;
unsigned long remainAugerPulse;
unsigned long initialPaid;
const float pulseAdj = 0.00555; //lbs of corn vended per ppr (360)
const float cornPrice = 2014.41; //(LBS/[per(1)rotation]/[how much shit comes out per counters];
const byte lcdAddr = 0x27;  // Address of I2C backpack
const byte lcdCols = 20;    // Number of character in a row
const byte lcdRows = 4;     // Number of lines
unsigned long last_dollar_time = 0;


volatile unsigned long timer_start;
volatile unsigned long pulse_time;
volatile unsigned long last_interrupt_time;
