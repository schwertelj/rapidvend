#include "def.h"
#include "Vend.h"
#include "Arduino.h"
#include "wire.h"
#include "LiquidCrystal_I2C.h"
#include "dollarCnt.h"
#include <avr/wdt.h>

LiquidCrystal_I2C lcd(lcdAddr,lcdCols,lcdRows);


void setup()
{
  // Pin setups for bill validator and button
  pinMode(billValidator, INPUT_PULLUP);//
  pinMode(handToggleSwitch, INPUT_PULLUP);//
  pinMode(augerPulse, INPUT_PULLUP);
  pinMode(augerEnable, OUTPUT);//
  pinMode(Output_PIN, OUTPUT);//
  pinMode(tiltSwitch, INPUT);
  digitalWrite(Output_PIN, LOW);  //new


  Serial.begin(115200);   // Initialize serial ports for communication.

  lcd.init();
  lcd.backlight();

  attachInterrupt(digitalPinToInterrupt(billValidator), dollarCnt, CHANGE);
  attachInterrupt(digitalPinToInterrupt(augerPulse), augerSub, RISING);
  delay(500); 
  wdt_enable(WDTO_250MS);

}

void loop()
{
toggleState = digitalRead(handToggleSwitch);
int tilt = digitalRead(tiltSwitch);
Serial.print(dollarCounter);


  if (paidAugerPulse < 0)   // Clearing counters for backlash
    {
      dollarCounter = 0;
      paidAugerPulse = 0;
      remainAugerPulse = 0;
      initialPaid = 0;
    }

  if (toggleState != lastToggleState && tilt == 1) // if switch is changed state, go to vend.h
    {
      lcd.clear();
      vend(); //  call vend funtion
    }
                 
  lcd.setCursor(5, 0);
  lcd.print(F("Welcome to"));
  lcd.setCursor(0, 1);
  lcd.print(F("     Rapid Vend     "));
  lcd.setCursor(1, 2);
  lcd.print(F("Please Insert Cash"));
  lcd.setCursor(1,3);
  lcd.print(F("Pounds left: "));
  lcd.print(paidAugerPulse*pulseAdj);

  if (paidAugerPulse*pulseAdj < 100)
  {
    lcd.setCursor(19,3);
    lcd.print(F(" "));
  }

  if (paidAugerPulse*pulseAdj < 10)
  {
    lcd.setCursor(18,3);
    lcd.print(F(" "));
  }

wdt_reset();

}
