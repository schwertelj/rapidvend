#include "wire.h"
#include <avr/wdt.h>
#include <util/atomic.h> // this library includes the ATOMIC_BLOCK macro.
#include <SPI.h>
#include "Adafruit_FRAM_SPI.h"
#include "Adafruit_SHT31.h"

//Defined Pins
#define billValidator 2
#define handToggleSwitch 5
#define augerPulse 3
#define augerEnable 9
#define tiltSwitch 7

// FRAM SPI Pin Definitions
uint8_t FRAM_CS = 10;
uint8_t FRAM_SCK= 13;
uint8_t FRAM_MISO = 12;
uint8_t FRAM_MOSI = 11;
Adafruit_FRAM_SPI fram = Adafruit_FRAM_SPI(FRAM_SCK, FRAM_MISO, FRAM_MOSI, FRAM_CS);

// FRAM Memory Addresses for system variables
const int PAID_PULSE_FRAM_ADDRESS = 1; // Memory address where paidAugerPulse is written to
const int CORN_PRICE_FRAM_ADDRESS = 100; // Memory addres where corn price is written to
const int PULSE_ADJ_PRICE_FRAM_ADDRESS = 200; // memory address where pulse adjust is written to


//Defined Variables
volatile unsigned long timeOutStart = millis();      // Time out counter for clearing out credits
unsigned long frozenTimeOutStart = millis();
unsigned long writeTimer = millis();        // Used to write to serial every 250ms 
unsigned long now = millis();               // Current time in milliseconds, re-evaluated every loop
int toggleState = 0;                        // The current state of the user toggle switch to dispense corn, digital input
int lastToggleState = 0;                    // Last processed state of the user toggle switch for dispensing corn
volatile long paidAugerPulse = 0;           // Number of auger pulses paid for that have not yet been dispensed
const unsigned long timeOut = 1200000;      // 20 minutes of inactivity clears credits
long frozenPulse = 0;
boolean temperatureSensorPresent = true;
float localTemperature = 0.0;
float localHumidity = 0.0;
Adafruit_SHT31 sht31 = Adafruit_SHT31();

// Variables unique to each RV machine:

// // --- RV1:
// double pulseAdj = 0.00475;            // lbs of corn vended per encoder pulse - Encoder is 1 pulse per degree, or 360 for a full rotation
// int cornPrice = 1650;                 // Auger pulses purchased per dollar
// const int tiltOK = 0;                       // Depending on the tilt sensor, the tilt switch is ok if it's 1 or 0

// --- RV2:
// double pulseAdj = 0.00640;            // lbs of corn vended per encoder pulse - Encoder is 1 pulse per degree, or 360 for a full rotation
// int cornPrice = 1225;                 // Auger pulses purchased per dollar
// const int tiltOK = 0;                       // Depending on the tilt sensor, the tilt switch is ok if it's 1 or 0

// --- RV3:
// double pulseAdj = 0.00360;            // lbs of corn vended per encoder pulse - Encoder is 1 pulse per degree, or 360 for a full rotation
// int cornPrice = 750;                 // Auger pulses purchased per dollar
// const int tiltOK = 1;                       // Depending on the tilt sensor, the tilt switch is ok if it's 1 or 0

// --- RV4:
double pulseAdj = 0.00640;            // lbs of corn vended per encoder pulse - Encoder is 1 pulse per degree, or 360 for a full rotation
int cornPrice = 1225;                 // Auger pulses purchased per dollar
const int tiltOK = 1;                       // Depending on the tilt sensor, the tilt switch is ok if it's 1 or 0

volatile unsigned long timer_start;         // Used in dollarCnt ISR to keep track of start time of pulse

void setup() {
  // Pin Initialization
  pinMode(billValidator, INPUT_PULLUP);     // Bill validator pulse
  pinMode(handToggleSwitch, INPUT_PULLUP);  // Toggle switch to start and stop dispensing of corn
  pinMode(augerPulse, INPUT_PULLUP);        // Auger encoder Pulse
  pinMode(augerEnable, OUTPUT);             // Enable auger motor (via SSR)
  pinMode(tiltSwitch, INPUT_PULLUP);        // Tilt switch to avoid plugging of the chute. Pulled high so is low when switch is closed (ok), high when clogged

  // Initialize Interrups for bill validator and auger encoder 
  attachInterrupt(digitalPinToInterrupt(billValidator), dollarCnt, CHANGE);
  attachInterrupt(digitalPinToInterrupt(augerPulse), augerSub, RISING);

  // if present, initialize temperature sensor
  if (temperatureSensorPresent) {
    if (!sht31.begin(0x44)) {
      temperatureSensorPresent = false;
    }
  }

  // Initialize serial ports for communication.
  Serial.begin(115200);

  while (!fram.begin()) {
    Serial.println("NO FRAM FOUND");
    delay(1000);
  }

  delay(1000); // Let things settle
  wdt_enable(WDTO_1S);

  // to forcibly reset pulse adjust value, uncomment below:
  // writePulseAdjust(pulseAdj);
  pulseAdj = readPulseAdjust();
  
  // to forcibly reset corn price, uncomment below:
  // writeCornPrice(cornPrice);
  cornPrice = readCornPrice(); // Read in corn price from memory

  // Initialize paidAugerPulse from memory
  paidAugerPulse = readPaidPulses(); // Read in paidAugerPulse from memory in the event of unexpected restart while vending
  // To forceably zero out the paid auger pulse, uncomment and flash. Then comment out and flash again. 
  // paidAugerPulse = 0;

  toggleState = digitalRead(handToggleSwitch);
  lastToggleState = digitalRead(handToggleSwitch);
}

void loop() {
  toggleState = digitalRead(handToggleSwitch);  // Current position of toggle switch
  int tilt = digitalRead(tiltSwitch);           // Position of tilt switch
  now = millis();

  
  ATOMIC_BLOCK(ATOMIC_RESTORESTATE) {
    frozenPulse = paidAugerPulse;
    frozenTimeOutStart = timeOutStart;
  }

  // Main Vending Logic:
  if (tilt == tiltOK) {
    // If tilt switch is closed (pulled to ground), go ahead with vending
    
    if (toggleState != lastToggleState) {
      // If toggle switch is on (in new state), start vending

      if (frozenPulse > 0) {
        // If there are credits availabe, turn on auger and dispense corn
        ATOMIC_BLOCK(ATOMIC_RESTORESTATE) {
          timeOutStart = now;                 // Reset time out countdown if it's dispensing
        }
        digitalWrite(augerEnable, HIGH);  // turn auger on

      } else {
        // if the paid auger pulses is 0, stop vending and toggle off

        lastToggleState = toggleState;      // Digitally consider the switch "off"
        digitalWrite(augerEnable, LOW);     // turn off auger
      }
    } else {
      // If toggle switch is off

      // timeout credit clearing logic
      if (frozenPulse > 0 && now > frozenTimeOutStart && now - frozenTimeOutStart > timeOut) {
        // If there are credits left that aren't being dispensed and the timeout amount has been reached, 
        // zero out remaining credits
        ATOMIC_BLOCK(ATOMIC_RESTORESTATE) {
          paidAugerPulse = 0;
        }
      } 

      // Machine is not set to vend
      digitalWrite(augerEnable, LOW);   // Assure auger is off
    }
  } else {
    // Otherwise, show warning that tilt switch is blocked
    ATOMIC_BLOCK(ATOMIC_RESTORESTATE) {
      timeOutStart = now;
    }
    lastToggleState = toggleState;    // Digitally consider the toggle switch "off"
    digitalWrite(augerEnable, LOW);   // turn off auger
  } 

  // Every 250ms, write state to serial, and check serial for updates
  if (now - writeTimer > 250) {

    if (readPaidPulses() != frozenPulse) {
      // if the paidAugerPulse value in memory is different from that in the program, change the value in memory
        writePaidPulses(frozenPulse);
    }

    // If timeout counter is running, record that too
    unsigned long timeOutCounter = 0;
    if (frozenPulse > 0) {
      timeOutCounter = now - frozenTimeOutStart;
    }

    // If new serial info is available, read it in
    if (Serial.available() > 0 && toggleState == lastToggleState) {

      String incomingValType; // The type of data we're about to read. Either "price" or "calibration"
      incomingValType = Serial.readStringUntil(':'); // Type is seperate from value by colon ':'

      String incomingVal; // The updated value, read as string for convenience, then parsed to numeric type
      incomingVal = Serial.readStringUntil('\n');

      if (incomingValType == "price") {
        // If type is price, calculate new cornPrice and adjust
        float newPrice = incomingVal.toFloat(); // Parse from string
        if (newPrice != 0) { 
          // value will only be zero if it was unable to parse the string
          cornPrice = int(1.0/newPrice  * 1.0/pulseAdj); // the value received is the price for 1lb of vended corn
          writeCornPrice(cornPrice); // write new value to memory
        }
      } else if (incomingValType == "calibration") {
        // If type is calibration, value will modify pulseAdj
        float newCal = incomingVal.toFloat();
        if (newCal != 0) {
          // the value received is the amount of corn vended for 1 dollar
          pulseAdj = newCal / cornPrice;
          writePulseAdjust(pulseAdj);
        }
      } else if (incomingValType == "payment") {
        int newPayment = incomingVal.toInt();
        if (newPayment != 0) {
          ATOMIC_BLOCK(ATOMIC_RESTORESTATE) {
            paidAugerPulse = paidAugerPulse + (cornPrice*newPayment); // increment amount of pulses that have been paid for
            writePaidPulses(paidAugerPulse); // write the new value to memory, in case a restart happens
          }
          timeOutStart = millis();
        }
      }
    }

    // take a temperature measurement
    if (temperatureSensorPresent) {
      localTemperature = sht31.readTemperature();
      localHumidity = sht31.readHumidity();
    }

    dumpStateToSerial(tilt, timeOutCounter, frozenPulse, localTemperature, localHumidity); // Write to serial
    writeTimer = millis();                   // Reset write timer
  }

  wdt_reset(); // Tell the watchdog we're still running
}

// ---------- Memory Operations ----------

// Write a long value to FRAM at the global memory address
void writeMemory(int addr, long totalVal) {
  fram.writeEnable(true);
  fram.write(addr, (const uint8_t*) &totalVal, sizeof(totalVal));
  fram.writeEnable(false);
}

// Read the (long) value from FRAM at the global memory address
long readMemory(int addr) {
  long readVal;
  fram.read(addr, (uint8_t*) &readVal, sizeof(readVal));
  return readVal;
}

void writePaidPulses(long newPaidPulses) {
  writeMemory(PAID_PULSE_FRAM_ADDRESS, newPaidPulses);
}

void writeCornPrice(int newCornPrice) {
  writeMemory(CORN_PRICE_FRAM_ADDRESS, (long) newCornPrice);
}

void writePulseAdjust(double newPulseAdj) {
  writeMemory(PULSE_ADJ_PRICE_FRAM_ADDRESS, (long) (newPulseAdj * 10000));
}

long readPaidPulses() {
  return readMemory(PAID_PULSE_FRAM_ADDRESS);
}

int readCornPrice() {
  return (int) readMemory(CORN_PRICE_FRAM_ADDRESS);
}

double readPulseAdjust() {
  return (double) (readMemory(PULSE_ADJ_PRICE_FRAM_ADDRESS) / 10000.0);
}

// ---------- ISRs ----------

// dollarCnt is an ISR for reading pulses from the Bill Validator system
// The pin is normall held high so pulses are low and expectected to be 50ms +/- 2ms
// When a pulse is validated, modify the value in paidAugerPulse
void dollarCnt() {
  if(digitalRead(billValidator) == LOW) {
    // Pin went low, start pulse timer
    timer_start = millis();
    // Note: Millis does not increment within the ISR!
  } else {
    // Pin went low -> high
    // validate pulse width
    volatile unsigned long pulse_time = ((volatile unsigned long)millis() - timer_start); // width of pulse in ms
    if (pulse_time > 47 && pulse_time < 53) { // Pulse should be 50ms, we'll leave some room for error on either side
      // If pulse width is valid, modify paidAugerPulse
      paidAugerPulse += cornPrice; // increment amount of pulses that have been paid for
      writePaidPulses(paidAugerPulse); // write the new value to memory, in case a restart happens
      timeOutStart = millis(); // reset timeout when new money is added
    }
  }
} 

// augerSub is an ISR for reading pulses from the auger encoder
// Each pulse decrements the value in paidAugerPulse by 1
// If the value goes negative, set it to 0 even
void augerSub() {
  paidAugerPulse--;
  if (paidAugerPulse <= 0) {
    paidAugerPulse = 0;
  }
}

// ---------- Serial Operations ----------

// dumpStateToSerial writes the global state to the serial port
// in a JSON format
void dumpStateToSerial(bool tilt, unsigned long timeOut, long pulses, float temp, float humid) {
  Serial.print("{'toggleState': ");
  Serial.print(toggleState);
  Serial.print(", 'lastToggleState': ");
  Serial.print(lastToggleState);
  Serial.print(", 'tilt': ");
  Serial.print(tilt);
  Serial.print(", 'paidAugerPulse': ");
  Serial.print(pulses);
  Serial.print(", 'timeOutCounter': ");
  Serial.print(timeOut);
  Serial.print(", 'pulseAdj': ");
  Serial.print(pulseAdj*1000);
  Serial.print(", 'cornPrice': ");
  Serial.print(cornPrice);
  Serial.print(", 'arduinoExecTime': ");
  Serial.print(millis());
  Serial.print(", 'temperature': ");
  Serial.print(temp);
  Serial.print(", 'humidity': ");
  Serial.print(humid);
  Serial.println("}");
}
