## Raspian Install and OS Setup
- Flash the normal raspbian desktop distribution to SD card
- Enable headless ssh for easiest setup:   
    - drop an empty file on `/boot` names `ssh` without extensions 
- Enable forced resolution for using adafruit display
    - https://learn.adafruit.com/adafruit-5-800x480-tft-hdmi-monitor-touchscreen-backpack/raspberry-pi-config
    - Add the following to `boot/config.txt`
        ```
        hdmi_group=2
        hdmi_mode=87
        hdmi_cvt=800 480 60 6 0 0 0
        hdmi_drive=1
        ```
- Rpi is ready to be booted up w/ or w/o a screen

## Software Dependencies
- Initial ssh connection can be done with `ssh pi@<ip-address>` where password if `raspberry`. Change this on initial login
- Setup MDB hat if necessary (see `../mdb/README.md`)
- Install Kivy (see `../gui/README.md`)
- Install Paho-MQTT Library `pip install paho-mqtt`
- Clone rapidvend source:
    ```
    git clone https://gitlab.com/schwertelj/rapidvend
    ```
- Setup service to automaticall run rapidvend program:
    - Move `dataLogger.service` to `/etc/systemd/system/` via 
        ```
        sudo cp dataLogger.service /etc/systemd/system
        ```
    - Enable via `sudo systemctl enable dataLogger.service`
    - Start service `sudo systemctl start dataLogger.service`