Install Kivy to RPi using tutorial here:
https://kivy.org/doc/stable/installation/installation-rpi.html

- Install Python 
    ```
    sudo apt update
    sudo apt install python3-setuptools git-core python3-dev
    ```
- Install Kivy Dependencies
    ```
    sudo apt update
    sudo apt install pkg-config libgl1-mesa-dev libgles2-mesa-dev \
   libgstreamer1.0-dev \
   gstreamer1.0-plugins-{bad,base,good,ugly} \
   gstreamer1.0-{omx,alsa} libmtdev-dev \
   xclip xsel libjpeg-dev
   ```
- Install SDL2
    ```
    sudo apt install libsdl2-dev libsdl2-image-dev libsdl2-mixer-dev libsdl2-ttf-dev
    ```
- Install Kivy
    ``` 
    python -m pip install kivy[base] 
    ```
- Sometimes the desktop taskbar does not autohide - set it to always be hidden with:
    ```
    sudo nano /etc/xdg/lxsession/LXDE-pi/autostart
    ```
    comment out the line `@lxpanel --profile LXDE-pi` with # symbol
    then restart
- To disable the startup wizard:
    ```
    sudo rm /etc/xdg/autostart/piwiz.desktop
    sudo reboot now
    ```