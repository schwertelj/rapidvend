import kivy
from kivy.app import App
from kivy.uix.floatlayout import FloatLayout
from kivy.clock import Clock
from kivy.core.window import Window

import paho.mqtt.client as mqtt

import time
from threading import Thread
import json
import logging
import serial
import serial.tools.list_ports

kivy.require('2.0.0')

# Arduino Serial Setup:
baudRate = 115200
serialName = 'Adafruit Metro 328'

# MQTT Setup
mqtt_ip = '75.72.148.232'
mqtt_port = 3318
rv_num = 'test'

# Tilt switch setup
tiltErrorState = 0 # either 1 or 0 if switch is opened or closed

# MDB (Bill Validator) Setup:
mdbValidator = True # Is the new type mdb validator used
# Map of binary error codes from validator to human translation
bv_errors = {
    0: "OK",
    1: "Defective Motor",
    2: "Sensor Problem",
    3: "Validator Busy",
    4: "Validator Checksum Error",
    5: "Validator Jammed",
    6: "Validator Reset",
    7: "Bill Removed",
    8: "Cash box out of position",
    9: "Validator Unavailable",
    10: "Invalid Escrow",
    11: "Bill Rejected",
    12: "Possible Bill Removal",
    13: "Bill Validator Unresponsive"
}


class test(FloatLayout):

    def __init__(self):
        super(test, self).__init__()
        Window.clearcolor = (1, 1, 1, 1) # Set for white background

        self.state = {
            'arduinoExecTime': 0, # how long has the arduino been alive for (in ms)
            'cornPrice': 0, # how many auger pulses are credited per dollar
            'lastToggleState': 0, # what was the last "on" state on the toggle switch
            'paidAugerPulse': 0,  # how many credits/pulses are paid for
            'piUpTime': round(time.time() * 1000), # when did the pie start execution
            'pulseAdj': 0, # internal calculation for pounds vended per auger pulse
            'tilt': abs(tiltErrorState - 1), # tilt switch state
            'timeOutCounter': 0, # time out counter that clears credits after 20 minutes
            'toggleState': 0, # current state of toggle switch. Vending is ON if it does not match last toggle state
            'ts': round(time.time() * 1000), # current time in epoch ms
            'bv_status': 0 # status of the bill validator, 0 is ok, otherwise its an error code intereger
        }

        # initialize a que to store incoming serial coms
        self.start = time.time()

        time.sleep(1)
        
        # The main display loop update
        self.update_clock = Clock.schedule_interval(self.update_data, (1/5))

        # Send mqtt heartbeat every 60 seconds regardless of operation state
        self.heartbeat_clock = Clock.schedule_interval(self.heartbeat, 60)

        # Setup serial connection and thread to talk with arduino
        port = self.getSerialPort()
        self.arduino_ser = serial.Serial(port, baudRate) # arduino serial connection
        get_serial_thread = Thread(target=self.get_serial)
        get_serial_thread.daemon = True
        get_serial_thread.start()

        if mdbValidator:
            # If a modern mdb bill validator is used, setup serial connection interface with it

            # Setup serial connection with bill validator / mdb interface hat
            self.bv_ser = serial.Serial() # Bill Validator serial connection
            self.bv_ser.baudrate = 115200 #Set the appropriate BaudRate
            self.bv_ser.timeout = 1
            self.bv_ser.port = '/dev/ttyAMA0' # mdb pi hat is always on this serial port
            self.bv_ser.open() #Open the serial connection

            self.bv_enable()

            # setup seperate thread for bv polling
            get_bv_serial_thread = Thread(target=self.get_bv_serial)
            get_bv_serial_thread.daemon = True
            get_bv_serial_thread.start()

        # subscribe to the rapidvend mqtt channel to allow remote price/calibration adjustments
        def on_connect(client, userdata, flags, rc):
            client.subscribe('rapidvend/'+rv_num+'/+')
        self.client = mqtt.Client()
        self.client.on_message = self.config_change
        self.client.on_connect = on_connect
        self.client.max_queued_messages_set(10)
        self.client.connect_async(mqtt_ip, mqtt_port, 60)
        self.client.loop_start()

    # ---------- Get Serial (Threaded) ----------
    # Grabs a line from serial comms
    # Should be a valued pair from arduino
    # Splits at comma and puts in queue
    def get_serial(self):
        while 1:
            
            self.state['ts'] = round(time.time() * 1000)
            updates = {}
            time.sleep(0.1) # Only scan every 100 ms

            waiting = 0
            try:
                if not self.arduino_ser.is_open:
                    print("trying to open serial port")
                    self.arduino_ser.open()
                    self.arduino_ser.flush()
                waiting = self.arduino_ser.in_waiting
            
            except Exception as e:
                print("Cannot find serial connection", e)
                print("Looking for new port")
                port = self.getSerialPort()
                if port != '':
                    self.arduino_ser.close()
                    self.arduino_ser = serial.Serial(port, baudRate)
                continue

            if(waiting >0):

                # Parse Incoming Data:
                line = self.arduino_ser.readline() # Line comes in as a byte array in json format
                line = line.decode()   # Read as bytes, immediately convert to string
                dataform = str(line).rstrip().replace('\'', '\"')
                if dataform == '': continue
                try:
                    updates = json.loads(dataform)              # Convert json string to dictionary
                except:
                    print("invalid message from arduino: ", line, updates)
                    continue

                # update state:
                for key, val in updates.items():
                    if key in self.state:
                        self.state[key] = val

                # If we got a bill validator pulse, send it as a one off
                if "billValidatorPulseWidth" in updates:
                    code = self.client.publish('rapidvend/'+rv_num, json.dumps({'billValidator': updates["billValidatorPulseWidth"], 'ts': self.state['ts']}), 1)
                    # print("SENT Bill UPDATE", code)

                if self.state['paidAugerPulse'] > 0 or self.state['tilt'] == tiltErrorState or self.state['lastToggleState'] != self.state['toggleState']:
                    # State has meaningfully changed, write an update
                    # logging.info(self.state)
                    code = self.client.publish('rapidvend/'+rv_num, json.dumps(self.state), 1)
                    # print("SENT UPDATE", code)
    
    # ---------- Get Bill Validator Serial (Threaded) ----------
    # Polls the bill validator via mdb every second to see if new bills have been recieved
    # If they have, send the payment amount to the arduino board for processing
    # function will only be called if mdbValidator is set to True
    def get_bv_serial(self):

        # Map of binary dollar codes to USD value
        dollar_mapping = {
            0: 1,
            1: 2,
            2: 5,
            3: 10,
            4: 20
        }

        bv_dead_count = 0 # keep track of how many times the bv has been unresponsive consecutively

        while True:
            # Poll the validator every 1 second
            time.sleep(1)

            if bv_dead_count > 10:
                # if bill validator has been unresponsive for a consecutive 10 seconds, throw error state
                self.state['bv_status'] = 13
                self.client.publish('rapidvend/'+rv_num, json.dumps({'ts': self.state['ts'], "bv_status": 13}))

            self.bv_ser.write(b'R,33\n') # Send 0x33 to bill validator to poll status
            b = self.bv_ser.readline() # read the result
            if b == b'':
                # if serial readline times out, keep track to make sure there isn't an error
                bv_dead_count += 1
                continue
            s = b.decode("utf-8")[2:-2] # result is a binary string in hexidecimal format, decode

            if s == "NACK":
                # If NACK is returned, bill validator is unreachable
                bv_dead_count +=1
                continue
            
        
            # if we made it this far, mdb + bill validator systems are alive and well
            # See if we had a bv dead count > 10 and need to re-init bill validator
            if bv_dead_count > 10:
                self.bv_enable()
            # then reset bv dead count
            bv_dead_count = 0

            if s == "ACK":
                # A simple ACK means validator has nothing new to report
                self.state['bv_status'] = 0
                continue

            codes = [s[i:i+2] for i in range(0,len(s), 2)] #otherwise, validator returns a serious of 4 byte status codes
            for idx, x in enumerate(codes):
                # Multiple codes can be recieved from bill validator in 4 byte chunks, process each code one by one

                # Binary representation of 4 byte hex code (i.e. 0x0f == '00001111') so we can dissect it
                bin_rep = bin(int(x, 16))[2:].zfill(8)

                if bin_rep[0] == '1':
                    # We recieved a valid bill
                    if bin_rep[1:4] != '000':
                        #bill was not stacked, don't count it
                        self.client.publish('rapidvend/'+rv_num, json.dumps({'ts': self.state['ts'], "bill_recieved_error": int(bin_rep[1:4], 2)}))
                        continue
                    
                    # Check bill dollar amount
                    dollar_amt = dollar_mapping[int(bin_rep[4:], 2)]
                    self.client.publish('rapidvend/'+rv_num, json.dumps({'ts': self.state['ts'], "payment": dollar_amt}))

                    # Send new credits to arduino
                    self.arduino_ser.write(str.encode("payment:"+str(dollar_amt)+"\n"))

                elif bin_rep[0] == '0':
                    # We recieved a status code, likely a rejected bill or validator is unavailable
                    self.state['bv_status'] = int(bin_rep[4:], 2)
                    self.client.publish('rapidvend/'+rv_num, json.dumps({'ts': self.state['ts']+(idx * 250), "bv_status": int(bin_rep[4:], 2)}))
    
    # initialize and enable bill validator
    def bv_enable(self):
        # Initial configuration of bill validator:
        # set mdb hat to master mode
        self.bv_ser.write(b'M,1')
        print('Master:', self.bv_ser.readline())

        # Reset the bill validator
        self.bv_ser.write(b'R,30\n')
        print('Reset:', self.bv_ser.readline())

        # Take the status of the bill validator (not needed for prod)
        # self.bv_ser.write(b'R,31\n')
        # print('Status', self.bv_ser.readline())

        # inital poll of bill validator, will return "just reset" status
        self.bv_ser.write(b'R,33\n')
        print('Poll', self.bv_ser.readline())

        # enable 1,2,5,10,20 bill denominations with no escrow
        self.bv_ser.write(b'R,34,00ff0000\n')
        print('Bill Enable', self.bv_ser.readline())

    # Scheduled every 60 seconds, this function sends a heartbeat of the current state to the mqtt
    # broker. Most useful when vending is not active to assure uptime
    def heartbeat(self, other):
        code = self.client.publish('rapidvend/'+rv_num, json.dumps(self.state), 1)
        # print("SENT HEARTBEAT", code, self.state)

    # When a configuration change is recieved over mqtt (price or calibration), send it via serial to the arduino
    def config_change(self, client, userdata, message):
        val = message.payload.decode()
        topic = message.topic.split('/')[-1]
        self.arduino_ser.write(str.encode(topic+':'+val+'\n'))

    # Update the display with the current state
    def update_data(self, other):
        cur_state = self.state

        # get pounds of corn paid for (unvended credits)
        lbs = cur_state.get("paidAugerPulse", 0) * cur_state.get("pulseAdj", 0) / 1000
        try:
            price = 1 / (cur_state.get("pulseAdj", 0) / 1000.0 * cur_state.get("cornPrice", 0))
        except:
            price = 0.11
        
        text = ""
        color = [0,0,0,1]

        # Visual States:

        # Normal vending operations:
        if cur_state["toggleState"] is not cur_state["lastToggleState"]:
            # User is trying to vend
            text = "Vending...\nLBS Left: {lbs:.2f}".format(lbs = lbs)
            color = [0,1,0,1]
        else:
            # User is not trying to vend
            text = "Welcome to Rapid Vend\nPlease Insert Cash\nLBS left: {lbs:.0f}\nPrice: ${price:.2f}/lb".format(lbs = lbs, price = price)
            color = [0,0,0,1]

        # Error states (takes priority)
        if cur_state["tilt"] is tiltErrorState:
            # Tilt switch error
            text = "Warning...\nChute is Plugged!\nUnplug to Continue\nThen toggle switch"
            color = [1,0,0,1]
        if cur_state["bv_status"] not in (0, 3, 9):
            # bill validator has error that is unexpected (0 = OK, 3 = busy, 9 = unavailable, all are normal)
            text = "Bill Reader Error\n"+bv_errors[cur_state["bv_status"]]
            color = [1,0,0,1]

        # apply color and text to UI
        self.ids.test_label.text = text
        self.ids.test_label.color = color
        # self.ids.debug.text = json.dumps(self.state)
    
    # Lookup arduino board via name to find the serial port, as it isn't always the 
    # same if either of the boards restarted
    def getSerialPort(self):
        ports = serial.tools.list_ports.comports()
        print("Serial Ports: ", [[port.product, port.name] for port in ports])
        for port in ports:
            if port.product == serialName:
                print('found port:', port.name)
                return '/dev/'+port.name
        else:
            print("no serial found")
            return ''

class testApp(App):
    def build(self):
        return test()

kv = testApp()
Window.fullscreen = True
kv.run()

