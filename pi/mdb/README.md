Using https://www.qiba.pt/products/pi-hat-plus/ to interface with MDB bill validator

## Setup Board Interface W/ Pi
(https://docs.qibixx.com/released/mdb-products/pihat-firststeps)
- In `sudo raspi-config -> Interface Options -> Serial Interface` Say `no` to login over serial and `yes` to hardware serial
- Remove `console=serial0,115200` from `boot/cmdline.text` if it exists
- `sudo systemctl disable hciuart`
- Find either `disable-bt.dtbo` or `pi3-disable-bt.dtbo` in the results of running `ls -la /boot/overlays/ | grep disable`
- Add the result (excluding file extension) as dtoverlay `boot/config.txt`
    ```
    dtoverlay=disable-bt
    enable_uart=1
    ```
- Full reboot of PI should enable full UART
- Make sure that GPIO pin 6 is pulled high:
    ```
    echo "6" >/sys/class/gpio/export # make io6 available
    echo "out" >/sys/class/gpio/gpio6/direction # make io6 an output
    echo "0" >/sys/class/gpio/gpio6/value # set low to reset
    sleep 1
    echo "in" >/sys/class/gpio/gpio6/direction # make io6 an input
    ```
- Note that sometimes the mdb hat doesn't respond to the first serial message for whatever reason. Don't condsider it dead until trying a couple of times.
- Note that if there are issues with the mdb hat, a firmware update may resolve things
    - https://docs.qibixx.com/released/mdb-products/firmware-update

## General Usage w/ Bill Validator
(see https://www.ccv.eu/wp-content/uploads/2018/05/mdb_interface_specification.pdf section 6)
- Setup MDB Hat in Master mode `M,1`
- Reset Bill Validator `R,30`
    - R = Write/Send
    - 30 is the hex code to be sent to the bill validator/mdb slave
- Poll the bill validator `R,33`
    - Response will include hex code `0x06` which means the bill validator just reset
- Enable bill types and disable Escrow `R,34,00050000`
    - Payload is broken down in two chunks
    - `0x0005` enables bill types 0-5, or 1, 2, 5, 10, 20 dollar bills
    - `0x0000` disables bill escrow, so bills are accepted (stacked) immediately