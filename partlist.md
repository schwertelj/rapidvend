| Part Name                 | Qty (Per RV Machine)  | Approx. Price | Function                                              | Link                                                              |
| ---------                 | --------------------  | ------------- | --------                                              | ----                                                              |
| RPi 4                     | 1                     | ??            | Bill Validator, Arduino, Display, and MQTT Interface  | lol                                                               | 
| RPi Antenna               | 1                     | $25           | Amplify wifi signal                                   | https://www.amazon.com/gp/product/B00H95C0A2/                     |
| RPi Din Mount             | 1                     | $15           | Mount RPi to DIN rail                                 | https://www.amazon.com/DIN-Rail-Mount-Raspberry-Pi/dp/B018J33308  |
| RPi MDB Interface         | 1                     | $150          | Interface bill validator to RPi                       | https://www.qiba.pt/products/pi-hat-plus/                         |
| CPI Talos Bill Validator  | 1                     | $350          | Validating and accepting bills                        | |
| PSU 24V 3A                | 1                     | $15           | Power for MDB loop                                    | https://www.amazon.com/SHNITPWR-100V-240V-Converter-Transformer-5-5x2-5mm/dp/B07Q29CD7J/ |
| HDMI Display 7" 800x480   | 1                     | $80           | Display to user                                       | https://www.adafruit.com/product/2406                             |
| HDMI Cable                | 1                     | $10           | Rpi -> Display                                        | https://www.amazon.com/gp/product/B00T58JH98/                     |
| Ruggeduino                | 1                     | $60           | Vending control                                       | https://www.rugged-circuits.com/microcontroller-boards/ruggeduino-et-extended-temperature-40c-85c |
| Arduino DIN Mount         | 1                     | $30           | DIN Mount for arduino computer w/ terminal breakout   | https://www.amazon.com/gp/product/B08LKYLK5G/                     |
| FRAM                      | 1                     | $10           | Non Volatile Memory for Arduino                       | https://www.adafruit.com/product/1897                             |
| 10 uF Capacitor           | 1                     | $1            | Disable Serial Auto-Reset on Arduino                  | 
| PSU 5v 10A                | 1                     | $25           | Main electronics psu                                  | https://www.amazon.com/gp/product/B0852HL336/                     |
| UPS                       | 1                     | $50           | Backup PSU                                            | https://www.amazon.com/gp/product/B01HDC236Q/                     |
| Encoder                   |
| SSR                       |
| Laser switches            |
| Toggle Switch             |
| DIN Rail, Terminal Blocks, 18awg Wire, etc |